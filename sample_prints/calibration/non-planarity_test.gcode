M420 S0					; turn off leveling matrix
G28 X0 Y0				; home X and Y
M109 R140				; wait for extruder to reach probe temp
M204 S300				; set probing acceleration
G29 V1					; start auto-leveling sequence
M420 S1					; turn on leveling matrix
M204 S2000				; restore standard acceleration
G1 X5 Y15 Z10 F5000			; move up off last probe point
G4 S1					; pause
M400					; wait for moves to finish
G28					; home
M117 Record value			; progress indicator message on LCD
M115
M119
M300 P20
G4 P20
M300 P20
G4 P20
M300 P20
