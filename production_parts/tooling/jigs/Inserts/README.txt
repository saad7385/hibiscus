The .stl's in this directory must be printed with a horizontal expansion value of -0.6mm for part fitment when printed in ABS. No supports should be necessary. -LF

The switch side double bearing holder insert jig and the X-belt tensioner insert jig do not require a horizontal expansion value. -LF
